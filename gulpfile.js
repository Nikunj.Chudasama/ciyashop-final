var gulp = require('gulp'),
    sass = require('gulp-sass')

    //compile
 gulp.task('sass', function () {
    gulp.src(['assets/scss/categories/**/*.scss', 'assets/scss/style.scss'])
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('assets/css'));
    });

//compile and watch
gulp.task('watch', function() {
    gulp.watch('assets/scss/**/*', ['sass']);
});

// default task
gulp.task('default', ['sass', 'watch']);

